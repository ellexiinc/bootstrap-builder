# Build bootstrap scss to css #

* Building custom bootstrap
* Automatically build scss to css
* Help to make custom css with Live server

---

## Prerequisites

* **Node.js(12.20.1)**
* **npm(7.19.1)**

## Getting started

### 1. Installation 

```sh
## Clone repository by git
git clone git@bitbucket.org:ellexiinc/bootstrap-builder.git

## Install package.json
cd bootstrap-builder
npm install
```

### 2. How to use

#### Bootstrap Builder
##### Usage

```
Usage: bs-build [options]

Options:
  -i, --input [FILEPATH]   input file for compile to css file
  -o, --output [FILEPATH]  output file to rendered css filePath
  -m, --minify             minify output css render file
  -h, --help               display help for command
```

##### Config default FilePath

```json
// config.json
{
    // Config default FilePath
    "inputFile":"./node_modules/bootstrap/scss/bootstrap.scss",
    "outputFile":"./custom-css/custom.css",
    "base":"./node_modules/bootstrap/scss",
    .
    .
    .
}
```

##### Example

```sh
npm run build
npm run build -- -m
npm run build -- -i ./examples/dist/custom.scss
npm run build -- -o ./examples/dist/custom.css
npm run build -- -i ./examples/dist/custom.scss -o ./examples/dist/css/custom.css -m
npm run build -- -b ./examples/dist/scss -i ./examples/dist/custom.scss -o ./examples/dist/css/custom.css -m
```

#### LiveServer
##### Usage

```
Usage: liveServer [options]

Options:
  -l, --liveServer [filePath]  Test your custom css with liveServer
  -h, --help                   display help for command
```

##### Config server

```json
// config.json
{
    .
    .
    .
    // Config Server status
    "port": 8181,
    "host": "0.0.0.0",
    "root": "./",
    "open": true,
    "wait": 500
}
```

##### Example

```sh
npm run liveServer -l
npm run liveServer -l ./examples/
```

---

## References

* [https://www.npmjs.com/package/sass](https://www.npmjs.com/package/sass)
