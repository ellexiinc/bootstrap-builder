var commander = require('commander');
var shell = require('shelljs');
var path = require('path');
var fs = require('fs-extra');

const program = new commander.Command();
var configFile = fs.readFileSync('./config.json', 'utf-8');
var config = JSON.parse(configFile);

if (!fs.existsSync("./custom-css/")){
    fs.mkdirSync("./custom-css/");
}

program
    .option('-i, --input [filePath]', 'input file for compile to css file', config.inputFile)
    .option('-o, --output [filePath]', 'output file to rendered css filePath', config.outputFile)
    .option('-b, --base [filePath]', 'Base for sass (ex - ./node_modules/bootstrap/scss)', config.base)
    .option('-m, --minify', 'minify output css render file', false);

program.parse(process.argv);
const options = program.opts();

var filePath = path.dirname(options.output);
var fileName = path.basename(options.output, 'css');

if (options.ipnut != undefined || filePath != undefined) {
    if (!(fs.existsSync(options.input))) {
        console.log("Please write correct Input Adress");
    } else if (!(fs.existsSync(filePath))) {
        console.log("Please write correct Output Adress");
    } else {
        if (fs.existsSync(options.base)) {
            if (options.minify == true) {
                shell.exec(`$(npm bin)/sass --watch --load-path=${options.base} --style=compressed ${options.input}:` + filePath + "/" + fileName + `min.css`);
            } else {
                shell.exec(`$(npm bin)/sass --watch --load-path=${options.base} ${options.input}:` + filePath + "/" + fileName + `css`);
            }
        } else {
            console.log("Please write correct Base Adress");
        }
    }
} else if (options.ipnut == undefined) {
    console.log("Please write Input Adress");
} else if (options.output == undefined) {
    console.log("Please write Output Adress");
}