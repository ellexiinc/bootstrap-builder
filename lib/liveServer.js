var commander = require('commander');
var fs = require('fs-extra');
var liveServer = require("live-server");

const program = new commander.Command();
var configFile = fs.readFileSync('./config.json', 'utf-8');
var config = JSON.parse(configFile);


program
    .option('-l, --liveServer [filePath]', 'Test your custom css with liveServer', config.root);

program.parse(process.argv);
const options = program.opts();

if (options.liveServer != undefined) {
    if (!(fs.existsSync(options.liveServer))) {
        console.log("Please write correct test file Path");
    } else {
        var params = {
            port: config.port,
            host: config.host,
            root: options.liveServer,
            open: config.open,
            wait: config.wait
        };
        liveServer.start(params);
    }
}